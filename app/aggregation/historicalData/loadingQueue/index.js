/**
 * Created by Suhas on 6/10/2016.
 */
var loadingQueueAggregation = require('./loadingQueue.js');
var elderlyLoadingQueue = require('./elderlyLoadingQueue.js');

module.exports={
    loadingQueueAggregation:loadingQueueAggregation,
    elderlyLoadingQueue:elderlyLoadingQueue
}